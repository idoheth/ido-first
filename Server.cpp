#include"Server.h"




void Server::myConfiguration()
{
	int error;
	error = WSAStartup(MAKEWORD(2, 2), &this->wsaData);
	if (error != 0) {
		cout << "WSAStartup failed with error: " << error << endl;
		system("pause");
		exit(1);
	}

	ZeroMemory(&this->hints, sizeof (this->hints));
	this->hints.ai_family = AF_INET;
	this->hints.ai_socktype = SOCK_STREAM;
	this->hints.ai_protocol = IPPROTO_TCP;
	this->hints.ai_flags = AI_PASSIVE;

	// Resolve the local address and port to be used by the server
	error = getaddrinfo(NULL, DEFAULT_PORT, &this->hints, &this->result);
	if (error != 0) {
		cout << "getaddrinfo failed: " << error << endl;
		WSACleanup();
		system("pause");
		exit(1);
	}
	cout << "configuration succeed!" << endl;
}

void Server::mysocketCreateAndConfig()
{
	this->ListenSocket = socket(this->result->ai_family, this->result->ai_socktype, this->result->ai_protocol);
	if (this->ListenSocket == INVALID_SOCKET) {
		cout << "Error at socket(): " << WSAGetLastError() << endl;
		freeaddrinfo(this->result);
		WSACleanup();
		system("pause");
		exit(1);
	}
	cout << "mysocketCreateAndConfig succeed!" << endl;
}

void Server::mybind()
{
	int error;
	error = bind(this->ListenSocket, this->result->ai_addr, (int)this->result->ai_addrlen);
	if (error == SOCKET_ERROR) {
		cout << "bind failed with error: " << WSAGetLastError() << endl;
		freeaddrinfo(this->result);
		closesocket(this->ListenSocket);
		WSACleanup();
		system("pause");
		exit(1);
	}
	else
	{
		cout << "bind seccesfull" << endl;
	}
	freeaddrinfo(result);
}

void Server::myListen()
{
	if (listen(this->ListenSocket, SOMAXCONN) == SOCKET_ERROR) {
		cout << "Listen failed with error: " << WSAGetLastError() << endl;
		closesocket(this->ListenSocket);
		WSACleanup();
		system("pause");
		exit(1);
	}
	else
	{
		cout << "listening on port 7777" << endl;
	}
}

SOCKET Server::myAccept()
{
	SOCKET tmp;
	tmp = accept(this->ListenSocket, NULL, NULL);
	if (tmp == INVALID_SOCKET) {
		cout << "accept failed: " << WSAGetLastError() << endl;
		closesocket(this->ListenSocket);
		WSACleanup();
		system("pause");
		exit(1);
	}
	else
	{
		this->ipAdd = inet_ntoa(this->clientInfo.sin_addr);
		cout << "acquired connection with: " << this->ipAdd << endl;
	}
	
	return tmp;
}

void Server::mySend(string data, SOCKET sock)
{
	int error;
	error = send(sock, data.c_str(), 1024, 0);
	if (error == SOCKET_ERROR)
	{
		printf("error\n");
		closesocket(sock);
		WSACleanup();
		system("pause");
		exit(1);
	}
}

void Server::myClose(SOCKET sock)
{
	int error;
	error = shutdown(sock, SD_SEND);
	if (error == SOCKET_ERROR) {
		cout << "shutdown failed: " << WSAGetLastError() << endl;
		closesocket(sock);
		WSACleanup();
		system("pause");
		exit(1);
	}
	else
	{
		cout << "shuting down" << endl;
		closesocket(sock);
		//WSACleanup();
	}
}

Server::Server()
{
	this->ListenSocket = INVALID_SOCKET;
	this->ClientSocket = INVALID_SOCKET;
	this->result = NULL;
	this->ptr = NULL;
	this->clientInfo = { 0 };
	this->size_ofC = sizeof(clientInfo);

	this->myConfiguration();
	this->mysocketCreateAndConfig();
	this->mybind();
	this->myListen();
}

void Server::start()
{
	SOCKET Csock;
	std::thread *the = nullptr;
	while (true)
	{
		Csock = this->myAccept();
		the = new std::thread(Server::dealWithClient, Csock);
	}

}

void Server::dealWithClient(SOCKET sock)
{
	Server::mySend("accepted!", sock);
	system("pause");
	Server::myClose(sock);
}