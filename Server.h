#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <winsock2.h>
#include <windows.h>
#include <iostream>
#include <fstream>
#include <string>
#include <ws2tcpip.h>
#include <thread>


#define DEFAULT_PORT "7777"

using std::cout;
using std::endl;
using std::string;



class Server
{
private:
	SOCKET ListenSocket; //= INVALID_SOCKET
	SOCKET ClientSocket; //= INVALID_SOCKET
	struct addrinfo *result, *ptr, hints;
	struct sockaddr_in clientInfo; //= { 0 };
	int size_ofC; //= sizeof(clientInfo);
	WSADATA wsaData;
	string ipAdd;

public:
	void myConfiguration();
	void mysocketCreateAndConfig();
	void mybind();
	void myListen();
	SOCKET myAccept();
	static void mySend(string data, SOCKET sock);
	static void myClose(SOCKET sock);
	Server();
	void start();
	static void dealWithClient(SOCKET sock);


};